package pizza.spring.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertTrue;

public class ResultPage {
    private WebDriver webDriver;

    public ResultPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        assertTrue(webDriver.findElement(By.cssSelector("body")).getText().contains("Récapitulatif de la commande"));
    }
}
