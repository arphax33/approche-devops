package pizza.spring.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.assertTrue;

public class CommandPage {
    private WebDriver webDriver;

    public CommandPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        assertTrue(webDriver.findElement(By.tagName("form")).isDisplayed());
    }
    public void fillForm(String pizza, String name, String phone) {
        Select pizzaSelect = new Select(webDriver.findElement(By.id("pizzaId")));
        WebElement nameInput = webDriver.findElement(By.id("nom"));
        WebElement phoneInput = webDriver.findElement(By.id("telephone"));

        pizzaSelect.selectByVisibleText(pizza);
        nameInput.sendKeys(name);
        phoneInput.sendKeys(phone);
    }

    public void fillForm(String name, String phone) {
        Select pizzaSelect = new Select(webDriver.findElement(By.id("pizzaId")));
        WebElement nameInput = webDriver.findElement(By.id("nom"));
        WebElement phoneInput = webDriver.findElement(By.id("telephone"));

        nameInput.sendKeys(name);
        phoneInput.sendKeys(phone);
    }

    public ResultPage sendForm() {
        WebElement submitButton = webDriver.findElement(By.tagName("button"));
        submitButton.click();
        return new ResultPage(webDriver);
    }

    public void sendFormWithError() {
        WebElement submitButton = webDriver.findElement(By.tagName("button"));
        submitButton.click();
    }
}
