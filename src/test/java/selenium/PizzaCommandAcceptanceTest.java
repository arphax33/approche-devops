package selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;
import pizza.spring.pages.CommandPage;
import pizza.spring.pages.HomePage;
import pizza.spring.pages.ResultPage;

import static org.junit.Assert.assertTrue;

public class PizzaCommandAcceptanceTest {

    private WebDriver webDriver;

    @Before
    public void createWebDriver() {
        webDriver = new OperaDriver();
    }

    @After
    public void closeWebDriver() {
        webDriver.quit();
    }

    @Test
    public void pizzaCommandIsSuccessful() throws Exception {
        String pizza = "Regina";
        String name = "Test";
        String phone = "0606060606";

        CommandPage commandPage= HomePage.openWith(webDriver)
                .clickOnCommand();
        commandPage.fillForm(pizza, name, phone);
        ResultPage resultPage = commandPage.sendForm();

        assertTrue(webDriver.findElement(By.tagName("body")).getText().contains(pizza));
        assertTrue(webDriver.findElement(By.tagName("body")).getText().contains(name));
        assertTrue(webDriver.findElement(By.tagName("body")).getText().contains(phone));
    }

    @Test
    public void pizzaCommandDisplayMessageErrorIfNoPizzaSelected() throws Exception {
        String errorMsg = "Vous devez choisir au moins une pizza";
        String name = "Test";
        String phone = "0606060606";

        CommandPage commandPage= HomePage.openWith(webDriver)
                .clickOnCommand();
        commandPage.fillForm(name, phone);
        commandPage.sendFormWithError();

        assertTrue(webDriver.findElement(By.tagName("body")).getText().contains(errorMsg));
    }

    @Test
    public void pizzaCommandDisplayMessageErrorIfNoPhoneGiven() throws Exception {
        String errorMsg = "ne peut pas être vide";
        String pizza = "Regina";
        String name = "Test";
        String phone = "";

        CommandPage commandPage= HomePage.openWith(webDriver)
                .clickOnCommand();
        commandPage.fillForm(pizza, name, phone);
        commandPage.sendFormWithError();

        assertTrue(webDriver.findElement(By.id("telephone.errors")).getText().contains(errorMsg));
    }
}
